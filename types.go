package trakt

import (
	"fmt"
	"time"
)

// ServiceType type of service.
type ServiceType string

// Service types.
const (
	ServiceTypeTrakt ServiceType = "trakt"
	ServiceTypeIMDB  ServiceType = "imdb"
	ServiceTypeTMDB  ServiceType = "tmdb"
	ServiceTypeTVDB  ServiceType = "tvdb"
)

// ItemType type of item.
type ItemType string

// Item types.
const (
	ItemTypeAll     ItemType = ""
	ItemTypeMovie   ItemType = "movie"
	ItemTypeShow    ItemType = "show"
	ItemTypeEpisode ItemType = "episode"
)

// Key identifies an item by type and id.
type Key struct {
	Type ItemType `json:"type"`
	ID   uint64   `json:"id"`
}

func (k *Key) String() string {
	if k.Type == ItemTypeAll {
		return ""
	}

	return fmt.Sprintf("%s.%d", k.Type, k.ID)
}

// Equal compares two keys for equality.
func (k *Key) Equal(k2 *Key) bool {
	return k.Type == k2.Type && k.ID == k2.ID
}

type ItemIDs struct {
	Slug  string `json:"slug"`
	Trakt uint64 `json:"trakt"`
	TvDB  uint64 `json:"tvdb"`
	Tmdb  uint64 `json:"tmdb"`
	Imdb  string `json:"imdb"`
}

// Movie represents a movie.
type Movie struct {
	Title string  `json:"title"`
	Year  uint16  `json:"year"`
	IDs   ItemIDs `json:"ids"`
}

// Show represents a show.
type Show struct {
	Title string  `json:"title"`
	Year  uint16  `json:"year"`
	IDs   ItemIDs `json:"ids"`
}

// Season represents a season.
type Season struct {
	Number   uint16    `json:"number"`
	Episodes []Episode `json:"episodes"`
	IDs      ItemIDs   `json:"ids"`
}

// Episode represents an episode.
type Episode struct {
	BaseEpisode
	FirstAired time.Time `json:"first_aired"`
}

// BaseEpisode represents the base set of fields for an episode.
type BaseEpisode struct {
	Season uint16  `json:"season"`
	Number uint16  `json:"number"`
	Title  string  `json:"title"`
	IDs    ItemIDs `json:"ids"`
}

// Item a movie, show, episode, person or list.
type Item struct {
	Type    ItemType `json:"type"`
	Movie   Movie    `json:"movie"`
	Show    Show     `json:"show"`
	Episode Episode  `json:"episode"`
}

// TraktID returns the Trakt.tv id of an item.
func (i *Item) TraktID() uint64 {
	switch i.Type {
	case ItemTypeMovie:
		return i.Movie.IDs.Trakt
	case ItemTypeShow:
		return i.Show.IDs.Trakt
	case ItemTypeEpisode:
		return i.Episode.IDs.Trakt
	case ItemTypeAll:
		return uint64(0)
	default:
		return uint64(0)
	}
}

// TraktKey Trakt.tv key (type and id).
func (i *Item) TraktKey() *Key {
	return &Key{Type: i.Type, ID: i.TraktID()}
}

func (i *Item) String() string {
	switch i.Type {
	case ItemTypeMovie:
		return fmt.Sprintf("%s (%d)", i.Movie.Title, i.Movie.Year)
	case ItemTypeShow:
		return fmt.Sprintf("%s (%d)", i.Show.Title, i.Show.Year)
	case ItemTypeEpisode:
		if i.Show.IDs.Trakt > 0 {
			return fmt.Sprintf("%s (%d) S%02dE%02d %s",
				i.Show.Title, i.Show.Year, i.Episode.Season, i.Episode.Number, i.Episode.Title)
		}

		return fmt.Sprintf("S%02dE%02d %s", i.Episode.Season, i.Episode.Number, i.Episode.Title)
	case ItemTypeAll:
		return fmt.Sprintf("%q", *i)
	default:
		return fmt.Sprintf("%q", *i)
	}
}

// HistoryItem a movie, show, season or episode from watch histroy.
type HistoryItem struct {
	Item

	ID        uint64    `json:"id"`
	WatchedAt time.Time `json:"watched_at"`
	Action    string    `json:"action"`
}

// ScrobbleItem a movie, show, season or episode from a scrobble response.
type ScrobbleItem struct {
	Item

	ID       uint64  `json:"id"`
	Action   string  `json:"action"`
	Progress float64 `json:"progress"`
}

// LastActivities represents the last activity of a user.
type LastActivities struct {
	All    time.Time `json:"all"`
	Movies struct {
		WatchedAt         time.Time `json:"watched_at"`
		CollectedAt       time.Time `json:"collected_at"`
		RatedAt           time.Time `json:"rated_at"`
		WatchlistedAt     time.Time `json:"watchlisted_at"`
		RecommendationsAt time.Time `json:"recommendations_at"`
		CommentedAt       time.Time `json:"commented_at"`
		PausedAt          time.Time `json:"paused_at"`
		HiddenAt          time.Time `json:"hidden_at"`
	} `json:"movies"`
	Episodes struct {
		WatchedAt     time.Time `json:"watched_at"`
		CollectedAt   time.Time `json:"collected_at"`
		RatedAt       time.Time `json:"rated_at"`
		WatchlistedAt time.Time `json:"watchlisted_at"`
		CommentedAt   time.Time `json:"commented_at"`
		PausedAt      time.Time `json:"paused_at"`
	} `json:"episodes"`
	Shows struct {
		RatedAt           time.Time `json:"rated_at"`
		WatchlistedAt     time.Time `json:"watchlisted_at"`
		RecommendationsAt time.Time `json:"recommendations_at"`
		CommentedAt       time.Time `json:"commented_at"`
		HiddenAt          time.Time `json:"hidden_at"`
	} `json:"shows"`
	Seasons struct {
		RatedAt       time.Time `json:"rated_at"`
		WatchlistedAt time.Time `json:"watchlisted_at"`
		CommentedAt   time.Time `json:"commented_at"`
		HiddenAt      time.Time `json:"hidden_at"`
	} `json:"seasons"`
	Comments struct {
		LikedAt   time.Time `json:"liked_at"`
		BlockedAt time.Time `json:"blocked_at"`
	} `json:"comments"`
	Lists struct {
		LikedAt     time.Time `json:"liked_at"`
		UpdatedAt   time.Time `json:"updated_at"`
		CommentedAt time.Time `json:"commented_at"`
	} `json:"lists"`
	Watchlist struct {
		UpdatedAt time.Time `json:"updated_at"`
	} `json:"watchlist"`
	Recommendations struct {
		UpdatedAt time.Time `json:"updated_at"`
	} `json:"recommendations"`
	Account struct {
		SettingsAt  time.Time `json:"settings_at"`
		FollowedAt  time.Time `json:"followed_at"`
		FollowingAt time.Time `json:"following_at"`
		PendingAt   time.Time `json:"pending_at"`
		RequestedAt time.Time `json:"requested_at"`
	} `json:"account"`
	SavedFilters struct {
		UpdatedAt time.Time `json:"updated_at"`
	} `json:"saved_filters"`
}

// Watched represents a watched movie, show, season or episode.
type Watched struct {
	Plays         uint16    `json:"plays"`
	LastWatchedAt time.Time `json:"last_watched_at"`
	LastUpdatedAt time.Time `json:"last_updated_at"`
	ResetAt       any       `json:"reset_at,omitempty"`
	Show          struct {
		Title string  `json:"title"`
		Year  uint16  `json:"year"`
		Ids   ItemIDs `json:"ids"`
	} `json:"show"`
	Seasons []struct {
		Number   uint16 `json:"number"`
		Episodes []struct {
			Number        uint16    `json:"number"`
			Plays         uint16    `json:"plays"`
			LastWatchedAt time.Time `json:"last_watched_at"`
		} `json:"episodes"`
	} `json:"seasons"`
	Movie struct {
		Title string  `json:"title"`
		Year  uint16  `json:"year"`
		Ids   ItemIDs `json:"ids"`
	} `json:"movie"`
}
