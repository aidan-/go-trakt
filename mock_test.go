package trakt

import (
	"bytes"
	"io"
	"net/http"
)

type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	return m.DoFunc(req)
}

func MockClientWithStaticHTTPResponse(resp []byte) *Client {
	c := &Client{
		http: &MockClient{
			DoFunc: func(req *http.Request) (*http.Response, error) {
				return &http.Response{
					StatusCode: 200,
					Body:       io.NopCloser(bytes.NewReader([]byte(resp))),
				}, nil
			},
		},
	}

	return c
}
