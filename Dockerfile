# Build image
FROM golang:1.20

# Install golangci-lint
RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh \
  | sh -s -- -b /usr/local/bin v1.52.2

# Set the working directory
WORKDIR /src

# Install dependencies
COPY . /src
RUN go get
