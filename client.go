package trakt

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const (
	traktURL = "https://api.trakt.tv"
)

// HTTPClient is a subset of the http.Client interface defining only the
// methods required.
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// Client wraps connection parameters for the Trakt.tv API.
type Client struct {
	http         HTTPClient
	baseURL      string
	clientID     string
	clientSecret string
	accessToken  string
}

// ClientConfig contains auth and connection details for the Trakt.tv API.
type ClientConfig struct {
	ClientID     string
	ClientSecret string
	AccessToken  string
}

// NewClient returns new Trakt.tv client.
func NewClient(options ClientConfig) *Client {
	return &Client{
		http:         &http.Client{},
		baseURL:      traktURL,
		clientID:     options.ClientID,
		clientSecret: options.ClientSecret,
		accessToken:  options.AccessToken,
	}
}

func (c *Client) performRequest(
	ctx context.Context,
	method string,
	path string,
	body io.Reader,
	result interface{},
) error {
	req, err := http.NewRequestWithContext(ctx, method, fmt.Sprintf("%s/%s", traktURL, path), body)
	if err != nil {
		return newError("failed to build request", method, fmt.Sprintf("%s/%s", c.baseURL, path), 0, false, err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", fmt.Sprintf("Go-Trakt v%s", Version))
	req.Header.Set("trakt-api-version", "2")
	req.Header.Set("trakt-api-key", c.clientID)

	if c.accessToken != "" {
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.accessToken))
	}

	resp, err := c.http.Do(req)
	if err != nil {
		return newError("failed to perform request", method, fmt.Sprintf("%s/%s", c.baseURL, path), 0, false, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		return newError("", method, fmt.Sprintf("%s/%s", c.baseURL, path), resp.StatusCode,
			resp.StatusCode == http.StatusTooManyRequests, nil)
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return newError("failed to read response body", method, fmt.Sprintf("%s/%s", c.baseURL, path), resp.StatusCode,
			false, err)
	}

	err = json.Unmarshal(respBody, result)
	if err != nil {
		return newError("failed to parse response body JSON", method, fmt.Sprintf("%s/%s", c.baseURL, path),
			resp.StatusCode, false, err)
	}

	return nil
}

func (c *Client) getRequest(ctx context.Context, path string, result interface{}) error {
	return c.performRequest(ctx, http.MethodGet, path, bytes.NewBuffer([]byte{}), result)
}

func (c *Client) postRequest(ctx context.Context, path string, body, result interface{}) error {
	data, err := json.Marshal(body)
	if err != nil {
		return newError("failed to convert request body to JSON", http.MethodPost, fmt.Sprintf("%s/%s", c.baseURL, path), 0,
			false, err)
	}

	return c.performRequest(ctx, http.MethodPost, path, bytes.NewBuffer(data), result)
}
