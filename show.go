package trakt

import (
	"context"
	"fmt"
)

// ShowService services wrapping the shows scope for a single show.
type ShowService struct {
	Client *Client
	ShowID uint64
}

// Show creates a new ShowService.
func (c *Client) Show(showID uint64) *ShowService {
	return &ShowService{Client: c, ShowID: showID}
}

// LastEpisode gets the most recently aired episode for a show.
func (s *ShowService) LastEpisode(ctx context.Context) (*BaseEpisode, error) {
	var episode BaseEpisode
	if err := s.Client.getRequest(ctx, fmt.Sprintf("shows/%d/last_episode", s.ShowID), &episode); err != nil {
		return nil, err
	}

	return &episode, nil
}
