package trakt

import (
	"context"
	"fmt"
)

// ScrobbleService services wrapping the scrobble scope.
type ScrobbleService struct {
	Client *Client
}

type scrobbleMovieRequest struct {
	Movie    Movie   `json:"movie"`
	Progress float64 `json:"progress"`
}

type scrobbleEpisodeRequest struct {
	Episode  Episode `json:"episode"`
	Progress float64 `json:"progress"`
}

// Scrobble creates a new ScrobbleService.
func (c *Client) Scrobble() *ScrobbleService {
	return &ScrobbleService{Client: c}
}

// Start watching in a media center.
func (s *ScrobbleService) Start(ctx context.Context, item *Item, progress float64) (*ScrobbleItem, error) {
	return s.request(ctx, "start", item, progress)
}

// Pause watching in a media center.
func (s *ScrobbleService) Pause(ctx context.Context, item *Item, progress float64) (*ScrobbleItem, error) {
	return s.request(ctx, "pause", item, progress)
}

// Stop watching in a media center.
func (s *ScrobbleService) Stop(ctx context.Context, item *Item, progress float64) (*ScrobbleItem, error) {
	return s.request(ctx, "stop", item, progress)
}

func (s *ScrobbleService) request(
	ctx context.Context,
	action string,
	item *Item,
	progress float64,
) (*ScrobbleItem, error) {
	var result ScrobbleItem

	var request interface{}

	switch item.Type {
	case ItemTypeMovie:
		request = scrobbleMovieRequest{Movie: item.Movie, Progress: progress}
	case ItemTypeEpisode:
		request = scrobbleEpisodeRequest{Episode: item.Episode, Progress: progress}
	case ItemTypeShow:
		return nil, newError(string(item.Type), "", "", 0, false, errScrobbleTypeUnsupported)
	case ItemTypeAll:
		return nil, newError(string(item.Type), "", "", 0, false, errScrobbleTypeUnsupported)
	default:
		return nil, newError(string(item.Type), "", "", 0, false, errScrobbleTypeUnsupported)
	}

	err := s.Client.postRequest(ctx, fmt.Sprintf("scrobble/%s", action), &request, &result)
	if err != nil {
		return &result, err
	}

	return &result, nil
}
