package trakt

import "context"

// OAuthService services wrapping the OAuth scope.
type OAuthService struct {
	Client      *Client
	RedirectURI string
}

// GetAccessTokenResponse response for a access token request.
type GetAccessTokenResponse struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
	AccessToken      string `json:"access_token"`
	TokenType        string `json:"token_type"`
	ExpiresIn        uint64 `json:"expires_in"`
	RefreshToken     string `json:"refresh_token"`
	Scope            string `json:"scope"`
	CreatedAt        uint64 `json:"created_at"`
}

// RefreshTokenResponse response for a refresh token request.
type RefreshTokenResponse struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
	AccessToken      string `json:"access_token"`
	TokenType        string `json:"token_type"`
	ExpiresIn        uint64 `json:"expires_in"`
	RefreshToken     string `json:"refresh_token"`
	Scope            string `json:"scope"`
	CreatedAt        uint64 `json:"created_at"`
}

type getAccessTokenRequest struct {
	Code         string `json:"code"`
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	RedirectURI  string `json:"redirect_uri"`
	GrantType    string `json:"grant_type"`
}

type refreshTokenRequest struct {
	RefreshToken string `json:"refresh_token"`
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	RedirectURI  string `json:"redirect_uri"`
	GrantType    string `json:"grant_type"`
}

// OAuth creates a new OAuthService.
func (c *Client) OAuth(redirectURI string) *OAuthService {
	return &OAuthService{Client: c, RedirectURI: redirectURI}
}

// GetAccessToken exchange code for access_token.
func (s *OAuthService) GetAccessToken(ctx context.Context, code string) (*GetAccessTokenResponse, error) {
	req := getAccessTokenRequest{
		Code:         code,
		ClientID:     s.Client.clientID,
		ClientSecret: s.Client.clientSecret,
		RedirectURI:  s.RedirectURI,
		GrantType:    "authorization_code",
	}

	var result GetAccessTokenResponse

	err := s.Client.postRequest(ctx, "oauth/token", &req, &result)
	if err != nil {
		return &result, err
	}

	if result.Error != "" {
		return &result, newError(result.Error, "", "", 0, false, errAuthFailed)
	}

	return &result, nil
}

// RefreshToken exchange refresh_token for access_token.
func (s *OAuthService) RefreshToken(ctx context.Context, refreshToken string) (*RefreshTokenResponse, error) {
	req := refreshTokenRequest{
		RefreshToken: refreshToken,
		ClientID:     s.Client.clientID,
		ClientSecret: s.Client.clientSecret,
		RedirectURI:  s.RedirectURI,
		GrantType:    "refresh_token",
	}

	var result RefreshTokenResponse

	err := s.Client.postRequest(ctx, "oauth/token", &req, &result)
	if err != nil {
		return &result, err
	}

	if result.Error != "" {
		return &result, newError(result.Error, "", "", 0, false, errAuthFailed)
	}

	return &result, nil
}
