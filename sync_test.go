package trakt

import (
	"context"
	"testing"
	"time"
)

func TestSyncLastActivities(t *testing.T) {
	httpResponse := []byte(`{
		"all": "2014-11-20T07:01:32.000Z",
		"movies": {
		  "watched_at": "2014-11-19T21:42:41.000Z",
		  "collected_at": "2014-11-20T06:51:30.000Z",
		  "rated_at": "2014-11-19T18:32:29.000Z",
		  "watchlisted_at": "2014-11-19T21:42:41.000Z",
		  "recommendations_at": "2014-11-19T21:42:41.000Z",
		  "commented_at": "2014-11-20T06:51:30.000Z",
		  "paused_at": "2014-11-20T06:51:30.000Z",
		  "hidden_at": "2016-08-20T06:51:30.000Z"
		},
		"episodes": {
		  "watched_at": "2014-11-20T06:51:30.000Z",
		  "collected_at": "2014-11-19T22:02:41.000Z",
		  "rated_at": "2014-11-20T06:51:30.000Z",
		  "watchlisted_at": "2014-11-20T06:51:30.000Z",
		  "commented_at": "2014-11-20T06:51:30.000Z",
		  "paused_at": "2014-11-20T06:51:30.000Z"
		},
		"shows": {
		  "rated_at": "2014-11-19T19:50:58.000Z",
		  "watchlisted_at": "2014-11-20T06:51:30.000Z",
		  "recommendations_at": "2014-11-20T06:51:30.000Z",
		  "commented_at": "2014-11-20T06:51:30.000Z",
		  "hidden_at": "2016-08-20T06:51:30.000Z"
		},
		"seasons": {
		  "rated_at": "2014-11-19T19:54:24.000Z",
		  "watchlisted_at": "2014-11-20T06:51:30.000Z",
		  "commented_at": "2014-11-20T06:51:30.000Z",
		  "hidden_at": "2016-08-20T06:51:30.000Z"
		},
		"comments": {
		  "liked_at": "2014-11-20T03:38:09.000Z",
		  "blocked_at": "2022-02-22T03:38:09.000Z"
		},
		"lists": {
		  "liked_at": "2014-11-20T00:36:48.000Z",
		  "updated_at": "2014-11-20T06:52:18.000Z",
		  "commented_at": "2014-11-20T06:51:30.000Z"
		},
		"watchlist": {
		  "updated_at": "2014-11-20T06:52:18.000Z"
		},
		"recommendations": {
		  "updated_at": "2014-11-20T06:52:18.000Z"
		},
		"account": {
		  "settings_at": "2020-03-04T03:38:09.000Z",
		  "followed_at": "2020-03-04T03:38:09.000Z",
		  "following_at": "2020-03-04T03:38:09.000Z",
		  "pending_at": "2020-03-04T03:38:09.000Z",
		  "requested_at": "2022-04-27T03:38:09.000Z"
		},
		"saved_filters": {
		  "updated_at": "2022-06-14T06:52:18.000Z"
		}
	  }`)

	s := MockClientWithStaticHTTPResponse(httpResponse)

	resp, err := s.Sync().GetLastActivities(context.TODO())
	if err != nil {
		t.Errorf("SyncService.LastActivities returned error: %v", err)
	}

	if resp == nil {
		t.Errorf("SyncService.LastActivities returned nil")
	}

	if resp.All != time.Date(2014, 11, 20, 7, 1, 32, 0, time.UTC) {
		t.Errorf("SyncService.LastActivities returned %+v, want %+v", resp.All, time.Date(2014, 11, 20, 7, 1, 32, 0, time.UTC))
	}
}

func TestGetWatched(t *testing.T) {
	httpResponse := []byte(`[
		{
		  "plays": 56,
		  "last_watched_at": "2014-10-11T17:00:54.000Z",
		  "last_updated_at": "2014-10-11T17:00:54.000Z",
		  "reset_at": null,
		  "show": {
			"title": "Breaking Bad",
			"year": 2008,
			"ids": {
			  "trakt": 1,
			  "slug": "breaking-bad",
			  "tvdb": 81189,
			  "imdb": "tt0903747",
			  "tmdb": 1396
			}
		  },
		  "seasons": [
			{
			  "number": 1,
			  "episodes": [
				{
				  "number": 1,
				  "plays": 1,
				  "last_watched_at": "2014-10-11T17:00:54.000Z"
				},
				{
				  "number": 2,
				  "plays": 1,
				  "last_watched_at": "2014-10-11T17:00:54.000Z"
				}
			  ]
			},
			{
			  "number": 2,
			  "episodes": [
				{
				  "number": 1,
				  "plays": 1,
				  "last_watched_at": "2014-10-11T17:00:54.000Z"
				},
				{
				  "number": 2,
				  "plays": 1,
				  "last_watched_at": "2014-10-11T17:00:54.000Z"
				}
			  ]
			}
		  ]
		},
		{
		  "plays": 23,
		  "last_watched_at": "2014-10-12T17:00:54.000Z",
		  "last_updated_at": "2014-10-12T17:00:54.000Z",
		  "show": {
			"title": "Parks and Recreation",
			"year": 2009,
			"ids": {
			  "trakt": 4,
			  "slug": "parks-and-recreation",
			  "tvdb": 84912,
			  "imdb": "tt1266020",
			  "tmdb": 8592
			}
		  },
		  "seasons": [
			{
			  "number": 1,
			  "episodes": [
				{
				  "number": 1,
				  "plays": 1,
				  "last_watched_at": "2014-10-11T17:00:54.000Z"
				},
				{
				  "number": 2,
				  "plays": 1,
				  "last_watched_at": "2014-10-11T17:00:54.000Z"
				}
			  ]
			},
			{
			  "number": 2,
			  "episodes": [
				{
				  "number": 1,
				  "plays": 1,
				  "last_watched_at": "2014-10-11T17:00:54.000Z"
				},
				{
				  "number": 2,
				  "plays": 1,
				  "last_watched_at": "2014-10-11T17:00:54.000Z"
				}
			  ]
			}
		  ]
		}
	  ]`)

	s := MockClientWithStaticHTTPResponse(httpResponse)

	resp, err := s.Sync().GetWatched(context.Background(), ItemTypeShow, true)
	if err != nil {
		t.Errorf("SyncService.GetWatch returned error: %v", err)
	}

	if resp == nil {
		t.Errorf("SyncService.GetWatch returned nil")
	}

	if resp[0].Plays != 56 {
		t.Errorf("SyncService.GetWatch returned %+v, want %+v", resp[0].Plays, 56)
	}
}
