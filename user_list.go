package trakt

import (
	"context"
	"fmt"
)

// UserListService services wrapping the list scope for a single list within the users scope for a single user.
type UserListService struct {
	Client   *Client
	UserID   string
	ListName string
}

type removeListItemRequestID struct {
	IDs ItemIDs `json:"ids"`
}

type removeListItemRequest struct {
	Movies   []removeListItemRequestID `json:"movies"`
	Shows    []removeListItemRequestID `json:"shows"`
	Seasons  []removeListItemRequestID `json:"seasons"`
	Episodes []removeListItemRequestID `json:"episodes"`
	People   []removeListItemRequestID `json:"people"`
}

type removeListItemResponseNotFoundIDs struct {
	IDs ItemIDs `json:"ids"`
}

type removeListItemResponse struct {
	Deleted struct {
		Movies   uint64 `json:"movies"`
		Shows    uint64 `json:"shows"`
		Seasons  uint64 `json:"seasons"`
		Episodes uint64 `json:"episodes"`
		People   uint64 `json:"people"`
	} `json:"deleted"`
	NotFound struct {
		Movies   []removeListItemResponseNotFoundIDs `json:"movies"`
		Shows    []removeListItemResponseNotFoundIDs `json:"shows"`
		Seasons  []removeListItemResponseNotFoundIDs `json:"seasons"`
		Episodes []removeListItemResponseNotFoundIDs `json:"episodes"`
		People   []removeListItemResponseNotFoundIDs `json:"people"`
	} `json:"not_found"`
}

// List creates a new UserListService.
func (s *UserService) List(name string) *UserListService {
	return &UserListService{Client: s.Client, UserID: s.UserID, ListName: name}
}

// Items items on a list.
func (s *UserListService) Items(ctx context.Context) ([]*Item, error) {
	var result []*Item

	err := s.Client.getRequest(ctx, fmt.Sprintf("users/%s/lists/%s/items?limit=9999999", s.UserID, s.ListName), &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

// RemoveItems removes items from a list.
func (s *UserListService) RemoveItems(ctx context.Context, traktKeys []*Key) (bool, error) {
	var result removeListItemResponse

	itemCount := uint64(0)
	request := removeListItemRequest{}

	for _, key := range traktKeys {
		idEntry := removeListItemRequestID{}
		idEntry.IDs.Trakt = key.ID

		switch key.Type {
		case ItemTypeMovie:
			request.Movies = append(request.Movies, idEntry)
		case ItemTypeShow:
			request.Shows = append(request.Shows, idEntry)
		case ItemTypeEpisode:
			request.Episodes = append(request.Episodes, idEntry)
		case ItemTypeAll:
			return false, newError(string(key.Type), "", "", 0, false, errItemTypeInvalid)
		default:
			return false, newError(string(key.Type), "", "", 0, false, errItemTypeInvalid)
		}

		itemCount++
	}

	err := s.Client.postRequest(ctx,
		fmt.Sprintf("users/%s/lists/%s/items/remove", s.UserID, s.ListName),
		&request,
		&result)
	if err != nil {
		return false, err
	}

	deleted := result.Deleted
	deletedCount := deleted.Movies + deleted.Shows + deleted.Seasons + deleted.Episodes + deleted.People

	nf := result.NotFound
	notFoundCount := len(nf.Movies) + len(nf.Shows) + len(nf.Seasons) + len(nf.Episodes) + len(nf.People)

	if deletedCount == itemCount && notFoundCount == 0 {
		return true, nil
	}

	return false, newError("", "", "", 0, false, errRemovalNotConfirmed)
}
