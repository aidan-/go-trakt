package trakt

import (
	"context"
	"fmt"
	"time"
)

// SyncService services wrapping the sync scope.
type SyncService struct {
	Client *Client
}

type addHistoryResponse struct {
	Added struct {
		Movies   uint64 `json:"movies"`
		Episodes uint64 `json:"episodes"`
	} `json:"added"`
	NotFound struct {
		Movies []struct {
			IDs ItemIDs `json:"ids"`
		} `json:"movies"`
		Shows []struct {
			IDs ItemIDs `json:"ids"`
		} `json:"shows"`
		Seasons []struct {
			IDs ItemIDs `json:"ids"`
		} `json:"seasons"`
		Episodes []struct {
			IDs ItemIDs `json:"ids"`
		} `json:"episodes"`
	} `json:"not_found"`
}

type historyMovie struct {
	Movie
	WatchedAt time.Time `json:"watched_at"`
}

type historyShow struct {
	Show
	WatchedAt time.Time `json:"watched_at"`
}

type historySeason struct {
	Season
	WatchedAt time.Time `json:"watched_at"`
}

type historyEpisode struct {
	Episode
	WatchedAt time.Time `json:"watched_at"`
}

type addHistoryRequest struct {
	Movies   []historyMovie   `json:"movies"`
	Shows    []historyShow    `json:"shows"`
	Seasons  []historySeason  `json:"seasons"`
	Episodes []historyEpisode `json:"episodes"`
}

type removeHistoryRequest struct {
	IDs []uint64 `json:"ids"`
}

type removeHistoryResponse struct {
	Deleted struct {
		Movies   uint64 `json:"movies"`
		Episodes uint64 `json:"episodes"`
	} `json:"deleted"`
	NotFound struct {
		IDs []uint64 `json:"ids"`
	} `json:"not_found"`
}

// Sync creates a new SyncService.
func (c *Client) Sync() *SyncService {
	return &SyncService{Client: c}
}

// GetWatched gets watched information for the provided type.
func (s *SyncService) GetWatched(ctx context.Context, itemType ItemType, seasons bool) ([]*Watched, error) {
	var result []*Watched

	if itemType != ItemTypeShow && itemType != ItemTypeMovie {
		return result, newError(string(itemType), "", "", 0, false, errItemTypeInvalid)
	}

	url := fmt.Sprintf("sync/watched/%ss", itemType)
	if !seasons {
		url += "?extended=noseasons"
	}

	err := s.Client.getRequest(ctx, url, &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

// GetLastActivities gets information about the most recent activity for a user.
func (s *SyncService) GetLastActivities(ctx context.Context) (*LastActivities, error) {
	var result LastActivities

	err := s.Client.getRequest(ctx, "sync/last_activities", &result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetHistory get watches of movie, show, season or episode by type and id.
func (s *SyncService) GetHistory(ctx context.Context, itemType ItemType, itemID uint64) ([]*HistoryItem, error) {
	var result []*HistoryItem

	if itemType == "" {
		return result, newError(string(itemType), "", "", 0, false, errItemTypeInvalid)
	}

	err := s.Client.getRequest(ctx, fmt.Sprintf("sync/history/%ss/%d?limit=9999999", itemType, itemID), &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

// GetHistoryAll get all watched movies, shows, seasons and episodes.
func (s *SyncService) GetHistoryAll(ctx context.Context, itemType ItemType) ([]*HistoryItem, error) {
	var result []*HistoryItem

	if itemType != "" {
		itemType += "s"
	}

	err := s.Client.getRequest(ctx, fmt.Sprintf("sync/history/%s?limit=9999999", itemType), &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

// AddHistory remove watched item from history.
func (s *SyncService) AddHistory(ctx context.Context, item *Item, watchedAt time.Time) (bool, error) {
	var request addHistoryRequest

	switch item.Type {
	case ItemTypeMovie:
		request.Movies = []historyMovie{{WatchedAt: watchedAt, Movie: item.Movie}}
	case ItemTypeShow:
		request.Shows = []historyShow{{WatchedAt: watchedAt, Show: item.Show}}
	case ItemTypeEpisode:
		request.Episodes = []historyEpisode{{WatchedAt: watchedAt, Episode: item.Episode}}
	case ItemTypeAll:
		return false, newError(string(item.Type), "", "", 0, false, errTypeUnsupported)
	default:
		return false, newError(string(item.Type), "", "", 0, false, errTypeUnsupported)
	}

	var result addHistoryResponse

	err := s.Client.postRequest(ctx, "sync/history", &request, &result)
	if err != nil {
		return false, err
	}

	if (result.Added.Movies + result.Added.Episodes) == 1 {
		return true, nil
	}

	return false, newError("", "", "", 0, false, errAddingNotConfirmed)
}

// RemoveHistory remove watched item from history.
func (s *SyncService) RemoveHistory(ctx context.Context, historyID uint64) (bool, error) {
	var result removeHistoryResponse

	err := s.Client.postRequest(ctx, "sync/history/remove", &removeHistoryRequest{IDs: []uint64{historyID}}, &result)
	if err != nil {
		return false, err
	}

	if (result.Deleted.Movies+result.Deleted.Episodes) == 1 && len(result.NotFound.IDs) == 0 {
		return true, nil
	}

	return false, newError("", "", "", 0, false, errRemovalNotConfirmed)
}
